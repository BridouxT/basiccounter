﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterLib;

namespace TestUnitaireBasicCounterLib
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TestGetterSetter()
        {

            Moncompteur.Setnb_compteur(5);
            Assert.AreEqual(5, Moncompteur.Getnb_compteur());


        }

        [TestMethod]
        public void Testplus()
        {
            Moncompteur.Setnb_compteur(5);
            Moncompteur.Plus();
            Assert.AreEqual(6, Moncompteur.Getnb_compteur());
            
        }

        [TestMethod]
        public void Testmoins()
        {
            Moncompteur.Setnb_compteur(5);
            Moncompteur.Moins();
            Assert.AreEqual(4, Moncompteur.Getnb_compteur());

        }

        [TestMethod]
        public void Testraz()
        {
          
            Moncompteur.Setnb_compteur(5);
            Moncompteur.Raz();
            Assert.AreEqual(0, Moncompteur.Getnb_compteur());

        }

    }
}
