﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterLib
{
    public class Moncompteur
    {
        private static int nb_compteur;

        public static void Plus()
        {
            nb_compteur++;
        }

        public static void Moins()
        {
            nb_compteur--;
        }

        public static void Raz()
        {
            nb_compteur = 0;
        }

        public static void Setnb_compteur(int nb)
        {
            nb_compteur = nb;

        }

        public static int Getnb_compteur()
        {
            return nb_compteur;
        }

    }
}
